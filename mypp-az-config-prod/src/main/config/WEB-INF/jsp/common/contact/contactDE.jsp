<%@ include file="/includes.jsp"%>

	<div id="TB_closeBar">
		<span id="TB_closeWindowButton" class="xbtnlink jq_dyn"	dyn_actions="tb_remove"></span>	
	</div>
	<div>
		<img src="${pageContext.request.contextPath}/img/logo.gif" alt border="0"></br></br>
		<h2>Haben Sie Fragen zu MediaCockpit ?</h2>
		<form name="newUserContactEdit" onsubmit="return postData(this,'${pageContext.request.contextPath}/contact.htm',serverResponse);">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td height="10"></td></tr>
	<tr>
		<td width="10"></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">		
				<tr><td class="subtit"></td></tr>
				<tr><td height="15"></td></tr>
				<tr>
					<td>
						<table border="0" cellpadding="1" cellspacing="0" width="100%">
							<tr>
								<td width="200" valign="top">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr><td>AZ Medien AG</td></tr>
										<tr><td>Aargauer Zeitung </td></tr>
										<tr><td>Support Rubrikenm�rkte</td></tr>
										<tr><td>Neumattstrasse 1</td></tr>
										<tr><td>CH-5001 Aarau</td></tr>
									</table>
								</td>
								<td width="200" valign="top">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td>Telefon&nbsp;</td>
											<td><strong>+41 58 200 53 63</strong></td>
										</tr>
										<tr>
											<td>Fax</td>
											<td>+41 58 200 53 54</td>
										</tr>
										<tr>
											<td>E-Mail</td>
											<td>a-z@azmedien.ch</td>
										</tr>
									</table>
								</td>
								<td width="200" valign="top">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr><td><strong>Service-Zeiten</strong></td></tr>
										<tr><td><strong>Mo-Fr 08h00-17h00</strong></td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td><hr></td></tr>
				<tr><td height="15"></td></tr>
					<tr><td>F�r allgemeine Fragen oder Nachrichten bitten wir Sie, das unten stehende Formular zu nutzen.</td></tr>
				<tr><td height="15"></td></tr>
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" width="400px">
							<tr>
								<td colspan="3">
									<textarea class="txt required" name="content" value="${contact.content}" rows="5" cols="400" style="width:500px;"></textarea>
								</td>
							</tr>
							<tr><td height="15"></td></tr>
							<tr>
								<td colspan="3">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr><td><div id="errorMessage"></div></tr></td>
									</table>
								</td>
							</tr>			
							<tr>
								<td class="txt" width="120px">Anrede</td>
								<td colspan="2">
									<mypp:select style="width:180px;" name="qualifier" data="${contact.qualifier}"><mypp:codelist>QUALIFIER</mypp:codelist></mypp:select>
								</td>
							</tr>
							<tr>
								<td class="txt" width="120">Firma</td>
								<td colspan="2">
									<input type="text" name="society" size="30" maxlength="30" value="${contact.society}"/>
								</td>
							</tr>	
							<tr>
								<td class="txt">Name&nbsp;<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif"></td>
								<td colspan="2">
									<input class="required string_1_30" type="text" name="lastName" size="30" maxlength="30" value="${contact.lastName}"/>
								</td>
							</tr>	
							<tr>
								<td class="txt">Vorname&nbsp;<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif"></td>
								<td colspan="2">
									<input class="required string_1_30" type="text" name="firstName" size="30" maxlength="30" value="${contact.firstName}">
								</td>
							</tr>								
							<tr>
								<td class="txt">Strasse</td>
								<td colspan="2">
									<input type="text" name="street" size="30" maxlength="30" value="${contact.street}">
								</td>
							</tr>	
							<tr>
								<td class="txt">PLZ <b>oder</b> Ort</td>
								<td colspan="2">
									<input type="text" name="locality" size="30" maxlength="30" value="${contact.locality}">
								</td>
							</tr>
							<tr>
								<td class="txt">E-Mail&nbsp;<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif"></td>
								<td>
									<input class="required" type="text" name="email" size="30" maxlength="60" value="${contact.email}">
								</td>
							</tr>
							<tr>
								<td class="txt">Telefon&nbsp;<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif"></td>
								<td colspan="2">
									<input class="required" type="text" name="phoneNumber" size="30" maxlength="30" value="${contact.phoneNumber}">
								</td>
							</tr>
							<tr>
								<td class="txt">Fax</td>
								<td colspan="2">
									<input type="text" name="faxNumber" size="30" maxlength="30" value="${contact.faxNumber}">
								</td>
							</tr>
							<tr>
								<td class="txt">Mobiltelefon</td>
								<td colspan="2">
									<input type="text" name="mobileNumber" size="30" maxlength="30" value="${contact.mobileNumber}">
								</td>
							</tr>	
						</table>
					</td>
				</tr>
				<tr><td height="15"></td></tr>
				<tr><td class="txt">Die mit einem (<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif">) bezeichneten Felder sind obligatorisch.</td></tr>
				<tr><td height="15"></td></tr>
				<tr></tr>
				<tr><td height="15"></td></tr>
			</table>
		</td>
		<td width="10"></td>
	</tr>
</table>
			<div class="buttonbar">
				<!-- DefaultButton -->
				<button type="submit" tabindex="2" class="btnDefault">
					<span class="btnRe">
						<span class="btnLi">
							<span><spring:message code="BUTTON.BTN_SEND" /></span>
						</span>
					</span>
				</button>
				<!-- DefaultButton end -->
				<!-- Button -->
				<button type="button" tabindex="1" onclick="tb_remove();" class="floatleft">
					<span class="btnRe">
						<span class="btnLi">
							<span><spring:message code="BUTTON.BTN_CANCEL" /></span>
						</span>
					</span>
				</button>
				<!-- Button end -->
            </div>
		</form>
	</div>
	
<script type="text/javascript">
	function serverResponse(data) {
		tb_remove();
	}
</script>