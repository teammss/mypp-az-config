<%@ include file="/includes.jsp"%>

	<div id="TB_closeBar">
		<span id="TB_closeWindowButton" class="xbtnlink jq_dyn"	dyn_actions="tb_remove"></span>	
	</div>
	<div>
		<img src="${pageContext.request.contextPath}/img/logo.gif" alt border="0"></br></br>
		<h2>Vous avez des questions concernant MediaCockpit ?</h2>
		<form name="newUserContactEdit" onsubmit="return postData(this,'${pageContext.request.contextPath}/contact.htm',serverResponse);">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td height="10"></td></tr>
	<tr>
		<td width="10"></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">		
				<tr><td class="subtit"></td></tr>
				<tr><td height="15"></td></tr>
				<tr>
					<td>
						<table border="0" cellpadding="1" cellspacing="0" width="100%">
							<tr>
								<td width="200" valign="top">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr><td>AZ Medien AG</td></tr>
										<tr><td>Aargauer Zeitung </td></tr>
										<tr><td>Support Rubrikenm�rkte</td></tr>
										<tr><td>Neumattstrasse 1</td></tr>
										<tr><td>CH-5001 Aarau</td></tr>
									</table>
								</td>
								<td width="200" valign="top">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td>T�l�phone&nbsp;</td>
											<td><strong>+41 58 200 53 63</strong></td>
										</tr>
										<tr>
											<td>Fax</td>
											<td>+41 58 200 53 54</td>
										</tr>
										<tr>
											<td>e-mail</td>
											<td>a-z@azmedien.ch</td>
										</tr>
									</table>
								</td>
								<td width="200" valign="top">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr><td><strong>Heures d'ouverture</strong></td></tr>
										<tr><td><strong>Lu-ve 08h00-17h00</strong></td></tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td><hr></td></tr>
				<tr><td height="15"></td></tr>
					<tr><td>Pour toute question ou communication d'ordre g�n�ral, nous vous prions de bien vouloir utiliser le formulaire ci-dessous.</td></tr>
				<tr><td height="15"></td></tr>
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0" width="400px">
							<tr>
								<td colspan="3">
									<textarea class="txt required" name="content" value="${contact.content}" rows="5" cols="400" style="width:500px;"></textarea>
								</td>
							</tr>
							<tr><td height="15"></td></tr>
							<tr>
								<td colspan="3">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr><td><div id="errorMessage"></div></tr></td>
									</table>
								</td>
							</tr>			
							<tr>
								<td class="txt" width="120px">Qualificatif</td>
								<td colspan="2">
									<mypp:select style="width:180px;" name="qualifier" data="${contact.qualifier}"><mypp:codelist>QUALIFIER</mypp:codelist></mypp:select>
								</td>
							</tr>
							<tr>
								<td class="txt" width="120">Soci�t�</td>
								<td colspan="2">
									<input type="text" name="society" size="30" maxlength="30" value="${contact.society}"/>
								</td>
							</tr>	
							<tr>
								<td class="txt">Nom&nbsp;<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif"></td>
								<td colspan="2">
									<input class="required string_1_30"  type="text" name="lastName" size="30" maxlength="30" value="${contact.lastName}"/>
								</td>
							</tr>	
							<tr>
								<td class="txt">Pr�nom&nbsp;<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif"></td>
								<td colspan="2">
									<input class="required string_1_30" type="text" name="firstName" size="30" maxlength="30" value="${contact.firstName}">
								</td>
							</tr>								
							<tr>
								<td class="txt">Rue</td>
								<td colspan="2">
									<input type="text" name="street" size="30" maxlength="30" value="${contact.street}">
								</td>
							</tr>	
							<tr>
								<td class="txt">NPA <b>ou</b> localit�</td>
								<td colspan="2">
									<input type="text" name="locality" size="30" maxlength="30" value="${contact.locality}">
								</td>
							</tr>
							<tr>
								<td class="txt">e-mail&nbsp;<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif"></td>
								<td>
									<input class="required" type="text" name="email" size="30" maxlength="60" value="${contact.email}">
								</td>
							</tr>
							<tr>
								<td class="txt">T�l�phone&nbsp;<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif"></td>
								<td colspan="2">
									<input class="required" type="text" name="phoneNumber" size="30" maxlength="30" value="${contact.phoneNumber}">
								</td>
							</tr>
							<tr>
								<td class="txt">Fax</td>
								<td colspan="2">
									<input type="text" name="faxNumber" size="30" maxlength="30" value="${contact.faxNumber}">
								</td>
							</tr>
							<tr>
								<td class="txt">T�l�phone mobile</td>
								<td colspan="2">
									<input type="text" name="mobileNumber" size="30" maxlength="30" value="${contact.mobileNumber}">
								</td>
							</tr>	
						</table>
					</td>
				</tr>
				<tr><td height="15"></td></tr>
				<tr><td class="txt">Les champs marqu�s d'un (<img width="12" height="12" alt="" src="${pageContext.request.contextPath}/img/icon_obligatory.gif">) sont obligatoires.</td></tr>
				<tr><td height="15"></td></tr>
				<tr></tr>
				<tr><td height="15"></td></tr>
			</table>
		</td>
		<td width="10"></td>
	</tr>
</table>
			<div class="buttonbar">
				<!-- DefaultButton -->
				<button type="submit" tabindex="2" class="btnDefault">
					<span class="btnRe">
						<span class="btnLi">
							<span><spring:message code="BUTTON.BTN_SEND" /></span>
						</span>
					</span>
				</button>
				<!-- DefaultButton end -->
				<!-- Button -->
				<button type="button" tabindex="1" onclick="tb_remove();" class="floatleft">
					<span class="btnRe">
						<span class="btnLi">
							<span><spring:message code="BUTTON.BTN_CANCEL" /></span>
						</span>
					</span>
				</button>
				<!-- Button end -->
            </div>
		</form>
	</div>
	
<script type="text/javascript">
	function serverResponse(data) {
		tb_remove();
	}
</script>